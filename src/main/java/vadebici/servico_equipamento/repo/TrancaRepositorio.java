package vadebici.servico_equipamento.repo;

import vadebici.servico_equipamento.db_in_memory.Database;
import vadebici.servico_equipamento.dom.*;
import vadebici.servico_equipamento.dom.dto.PostTranca;
import vadebici.servico_equipamento.dom.dto.TrancaNaRede;
import vadebici.exception.GenericApiException;

import java.util.*;
import java.util.stream.Collectors;

public class TrancaRepositorio {
    public Tranca salvarTranca(PostTranca postTranca) {
        Tranca tranca = new Tranca();
        String id = UUID.randomUUID().toString();
        tranca.setId(id);
        tranca.setStatus(postTranca.getStatus());
        tranca.setLocalizacao(postTranca.getLocalizacao());
        tranca.setModelo(postTranca.getModelo());
        tranca.setAnoDeFabricacao(postTranca.getAnoDeFabricacao());
        tranca.setNumero(String.valueOf(postTranca.getNumero()));
        Database.TRANCA_TABLE.put(id, tranca);
        return tranca;
    }

    public List<Tranca> buscarTrancas() {
        return Database.TRANCA_TABLE.keySet()
                .stream()
                .map(Database.TRANCA_TABLE::get)
                .collect(Collectors.toList());
    }

    public Tranca buscarTranca(String id) {
        Tranca tranca = Database.TRANCA_TABLE.get(id);
        if (tranca == null) throw new GenericApiException(404, "Tranca não encontrado");
        return tranca;
    }

    public Tranca atualizarTranca(String id, Tranca novaTranca){
        buscarTranca(id);
        Database.TRANCA_TABLE.replace(id, novaTranca);
        return novaTranca;
    }

    public void deletarTranca(String id){
        Tranca tranca = buscarTranca(id);
        if (tranca.getIdBicicleta() != null) throw new GenericApiException(422, "Tranca não pode ser removida poís possui uma bicicleta.");
        Database.TRANCA_TABLE.remove(id);
    }

    public Tranca alterarStatus(String id, String status){
        Tranca tranca = buscarTranca(id);
        tranca.setStatus(TrancaStatus.valueOf(status));
        return Database.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    private Totem buscarTotem(String id) {
        return Optional.ofNullable(Database.TOTEM_TABLE.get(id))
                .orElseThrow(() -> new GenericApiException(422, "Totem não encontrado."));
    }

    public void integrarNaRede(TrancaNaRede trancaNaRede) {
        Tranca tranca = buscarTranca(trancaNaRede.getIdTranca());
        Totem totem = buscarTotem(trancaNaRede.getIdTotem());
        tranca.setIdTotem(totem.getId());
        tranca.setStatus(TrancaStatus.DISPONIVEL);
        Database.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    public void removerDaRede(TrancaNaRede trancaNaRede) {
        Tranca tranca = buscarTranca(trancaNaRede.getIdTranca());
        if (tranca.getIdBicicleta() != null) throw new GenericApiException(422, "Tranca possui uma bicicleta.");
        buscarTotem(trancaNaRede.getIdTotem());
        tranca.setIdTotem(null);
        Database.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    public Bicicleta buscarBicicletaPorTranca(String id) {
        Tranca tranca = buscarTranca(id);
        return Optional.ofNullable(Database.BICICLETA_TABLE.get(tranca.getIdBicicleta()))
                .orElseThrow(() -> new GenericApiException(422, "Bicicleta não encontrado."));
    }
}
