package vadebici.servico_equipamento.controller;

import vadebici.servico_equipamento.dom.Email;
import vadebici.servico_equipamento.dom.Tranca;
import vadebici.servico_equipamento.dom.Bicicleta;
import vadebici.servico_equipamento.dom.dto.PostTranca;
import vadebici.servico_equipamento.dom.dto.TrancaNaRede;
import vadebici.servico_equipamento.repo.TrancaRepositorio;
import vadebici.servico_equipamento.service.ServicoExterno;
import vadebici.servico_equipamento.util.Validacao;
import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;

import java.util.List;


public class TrancaController {

    private TrancaController(){}
    public static final TrancaRepositorio trancaRepositorio = new TrancaRepositorio();
    private static final String TRANCA_ID = "idTranca";

    public static ServicoExterno servicoExterno =  new ServicoExterno();

    public static void salvarTranca(Context ctx) {
        String body = ctx.body();
        PostTranca postTranca = JavalinJson.getFromJsonMapper().map(body, PostTranca.class);
        Validacao.validarCampos(postTranca);
        Tranca tranca = trancaRepositorio.salvarTranca(postTranca);
        String response = JavalinJson.toJson(tranca);
        ctx.result(response);
        ctx.status(201);
    }

    public static void buscarTrancas(Context ctx) {
        List<Tranca> trancaList = trancaRepositorio.buscarTrancas();
        String response = JavalinJson.toJson(trancaList);
        ctx.result(response);
        ctx.status(200);
    }

    public static void buscarTranca(Context ctx) {
        String trancaId = ctx.pathParam(TRANCA_ID);
        Tranca tranca = trancaRepositorio.buscarTranca(trancaId);
        String response = JavalinJson.toJson(tranca);
        ctx.result(response);
        ctx.status(200);
    }

    public static void atualizarTranca(Context ctx) {
        String body = ctx.body();
        String id = ctx.pathParam(TRANCA_ID);
        Tranca tranca = JavalinJson.getFromJsonMapper().map(body, Tranca.class);
        Tranca resultado = trancaRepositorio.atualizarTranca(id, tranca);
        Validacao.validarCampos(tranca);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resultado));
    }

    public static void deletarTranca(Context ctx) {
        String id = ctx.pathParam(TRANCA_ID);
        trancaRepositorio.deletarTranca(id);
        ctx.status(200);
    }

    public static void buscaBicicletaPorTranca(Context ctx) {
        String id = ctx.pathParam(TRANCA_ID);
        Bicicleta bicicleta = trancaRepositorio.buscarBicicletaPorTranca(id);
        String response = JavalinJson.getToJsonMapper().map(bicicleta);
        ctx.status(200);
        ctx.result(response);
    }

    public static void alterarStatus(Context ctx) {
        String id = ctx.pathParam(TRANCA_ID);
        String status = ctx.pathParam("acao");
        Tranca tranca = trancaRepositorio.alterarStatus(id, status);
        String response = JavalinJson.getToJsonMapper().map(tranca);
        ctx.status(200);
        ctx.result(response);
    }

    public static void integrarNaRede(Context ctx) {
        String body = ctx.body();
        TrancaNaRede trancaNaRede = JavalinJson.getFromJsonMapper().map(body, TrancaNaRede.class);
        Validacao.validarCampos(trancaNaRede);
        trancaRepositorio.integrarNaRede(trancaNaRede);
        enviaEmail("erick.m.val@edu.unirio.br", "Tranca incluída na rede");
        ctx.status(200);
    }

    public static void removerDaRede(Context ctx) {
        String body = ctx.body();
        TrancaNaRede trancaNaRede = JavalinJson.getFromJsonMapper().map(body, TrancaNaRede.class);
        Validacao.validarCampos(trancaNaRede);
        trancaRepositorio.removerDaRede(trancaNaRede);
        enviaEmail("erick.m.val@edu.unirio.br", "Tranca removida da rede");
        ctx.status(200);
    }

    public static void enviaEmail(String destinatario, String mensagem) {
        Email email = new Email();
        email.setEnderecoEmail(destinatario);
        email.setMensagem(mensagem);
        servicoExterno.enviaEmail(email);
    }
}
