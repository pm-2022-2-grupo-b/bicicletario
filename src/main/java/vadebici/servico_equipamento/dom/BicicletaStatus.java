package vadebici.servico_equipamento.dom;

public enum BicicletaStatus {
    DISPONIVEL, APOSENTADA, NOVA, OCUPADA, MANUTENCAO
}
