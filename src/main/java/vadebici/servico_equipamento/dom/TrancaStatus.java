package vadebici.servico_equipamento.dom;

public enum TrancaStatus {
    DISPONIVEL, OCUPADA, MANUTENCAO, APOSENTADA, NOVA
}
