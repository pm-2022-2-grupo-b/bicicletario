package vadebici.servico_externo.service.mapping;

import java.util.Map;

public class GetnetRequisicaoShippingInfo {
    private Map<Object, Object> address;

    public Map<Object, Object> getAddress() {
        return address;
    }

    public void setAddress(Map<Object, Object> address) {
        this.address = address;
    }
}
