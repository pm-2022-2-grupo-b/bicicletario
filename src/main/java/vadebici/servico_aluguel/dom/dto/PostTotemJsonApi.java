package vadebici.servico_aluguel.dom.dto;

public class PostTotemJsonApi {
    private String localizacao;

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
}
